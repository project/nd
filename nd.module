<?php

/**
 * @file
 * Main node displays file.
 */

/**
 * Constants for content field default values
 */
define('ND_DEFAULT_LABEL_FORMAT', 'hidden');
define('ND_DEFAULT_REGION', 'disabled');
define('ND_DEFAULT_FORMAT', 'default');

/**
 * Constants for field types.
 */
define('ND_FIELD_THEME', -2);
define('ND_FIELD_FUNCTION', -1);
define('ND_FIELD_PREPROCESS', 0);
define('ND_FIELD_IGNORE', 1);
define('ND_FIELD_CUSTOM', 2);
define('ND_FIELD_OVERRIDABLE', 3);
define('ND_FIELD_OVERRIDDEN', 4);

/**
 * Implementation of hook_nodeapi().
 */
function nd_nodeapi(&$node, $op, $teaser = NULL, $page = NULL) {

  switch ($op) {

    // Add has body property.
    case 'load':
      $node_type = node_get_types('type', $node->type);
      $node->has_body = $node_type->has_body;
      break;

    // Determine build mode.
    case 'view':
      if ($node->build_mode === NODE_BUILD_PREVIEW) {
        $node->build_mode = $teaser == TRUE ? 'teaser' : 'full';
        $node->has_body = node_get_types('type', $node->type)->has_body;
      }
      else if ($node->build_mode === NODE_BUILD_NORMAL) {
        if (isset($node->view)) {
          if ($teaser == TRUE) {
            $node->build_mode = 'teaser';
          } else {
            $node->build_mode = 'full';
          }
        }
        else {
          $build_mode = ($page) ? 'full' : 'teaser';
          $node->build_mode = $build_mode;
        }
      }

      if ($node->build_mode == 'teaser' && $node->sticky == 1) {
        $node->build_mode = 'nd_sticky';
      }
      break;

    // Alter the node object for viewing.
    case 'alter':

      // Add a ND property to the node.
      $node->rendered_by_nd = FALSE;

      // See if css and rendering is needed later on by the preprocess hook.
      // There are two ways of excluding: global exclude on content type
      // or per build mode per content type.
      // We don't return here, because we want to add all fields on the node object
      // so themers can use it in their template.
      $exclude_build_modes = variable_get('nd_buildmodes_exclude', array());
      $exclude = ((isset($exclude_build_modes[$node->type][$node->build_mode])
        && $exclude_build_modes[$node->type][$node->build_mode] == TRUE) ||
        variable_get('nd_contenttype_'. $node->type, FALSE) == TRUE) ? TRUE : FALSE;
      if ($exclude == TRUE) {
        $node->do_not_render[$node->build_mode] = TRUE;
      }
      else {
        $node->rendered_by_nd = TRUE;
        if (variable_get('nd_regions_css', TRUE)) {
          drupal_add_css(drupal_get_path('module', 'nd') .'/css/regions.css');
        }
      }

      $regions = array();
      $nd_fields = array();
      $nd_display_settings = variable_get('nd_display_settings_'. $node->type, array());

      // Extra fields through code.
      $fields = nd_get_fields($node->type, $node->has_body, $node->build_mode);
      if (!empty($fields)) {
        foreach ($fields as $key => $field) {

          // Exclude now depends on the region, previous it was an exclude property.
          $region = _nd_default_value($nd_display_settings, $node->build_mode, 'fields', $key, 'region', 'disabled');

          if ($region != 'disabled') {

            $weight = _nd_default_value($nd_display_settings, $node->build_mode, 'fields', $key, 'weight', -19);
            $regions[$region][$key] = $weight;
            $node->content[$key]['#weight'] = $weight;
            $nd_fields[$key] = $key;

            // If the field has the storage key, it means the theming is done in that module.
            if (isset($field['storage'])) {
              continue;
            }

            // Theming can either be done in preprocess, or with a custom funtion or an
            // existing formatter theming function. For a custom function we'll pass
            // on the $node object and the field name and the complete field definition.
            switch ($field['type']) {
              case ND_FIELD_PREPROCESS:
              case ND_FIELD_IGNORE:
                break;
              case ND_FIELD_CUSTOM:
              case ND_FIELD_OVERRIDABLE:
              case ND_FIELD_OVERRIDDEN:
                _nd_render_code($key, $field, $node);
                break;
              case ND_FIELD_FUNCTION:
                if (isset($field['file'])) {
                  include_once($field['file']);
                }
                $function = _nd_default_value($nd_display_settings, $node->build_mode, 'fields', $key, 'format', array_shift($field['formatters']));
                $function($node, $key, $field);
                break;
              case ND_FIELD_THEME:
                $format = _nd_default_value($nd_display_settings, $node->build_mode, 'fields', $key, 'format', array_shift($field['formatters']));
                theme($format, $node);
                break;
            }
          }
        }
      }

      // Create some extra node keys.
      $node->nd_fields = $nd_fields;
      if (count($regions) > 0) {
        $node->regions = $regions;
      }
      break;
  }
}

/**
 * Implementation of moduleName_preprocess_hook().
 * The node data will be rendered in regions. This uses a helper function
 * so themers/developers can call that helper function from within
 * their preprocess_hooks if they are fiddling with some data. For information
 * about this decision see http://drupal.org/node/570592 (issue) and
 * http://drupal.org/node/572614 for information on howto implement.
 */
function nd_preprocess_node(&$vars, $hook) {
  if (!variable_get('nd_preprocess_override', FALSE)) {
    _nd_preprocess_node($vars, $hook);
  }
}

/**
 * Helper function used in either nd_preprocess_node or other preprocess function.
 */
function _nd_preprocess_node(&$vars, $hook) {

  $node = $vars['node'];

  // Add node-content_type-build mode suggestion.
  $vars['template_files'][] = 'node-'. $node->type .'-'. $node->build_mode;
  $vars['template_files'][] = 'node-'. $node->type .'-'. $node->build_mode .'-'. $node->nid;

  // Break all the rendering if needed. The do_not_render property is set in nodeapi above.
  if (isset($node->do_not_render[$node->build_mode])) {
    return;
  }

  // Also quit when there are no regions found.
  if (!isset($vars['regions'])) {
    $vars['content'] = '';
    return;
  }

  $regions = $vars['regions'];
  $themed_regions = array();
  $all_regions = nd_regions();
  $nd_display_settings = variable_get('nd_display_settings_'. $node->type, array());

  // First of all, create node->content[$field]['#value'] & ..['#weight'] keys
  // to render on later. Only take fields which have preprocess keys.
  $fields = nd_get_fields($node->type, $node->has_body, $node->build_mode);
  if (!empty($fields)) {
    foreach ($fields as $key => $field) {
      if ($field['type'] == ND_FIELD_PREPROCESS && array_key_exists($key, $node->nd_fields)) {
        $vars['node']->content[$key]['#value'] = '';
        if (isset($field['div'])) {
          $vars['node']->content[$key]['#value'] .= '<div class="'. $field['div'] .'">';
        }
        $vars['node']->content[$key]['#value'] .= $vars[$key];
        if (isset($field['div'])) {
          $vars['node']->content[$key]['#value'] .= '</div>';
        }
        $vars['node']->content[$key]['#weight'] = _nd_default_value($nd_display_settings, $node->build_mode, 'fields', $key, 'weight', -19);
        unset($vars[$key]);
      }
    }
  }

  // loop through all regions and store some classes
  $region_classes = array();
  foreach ($all_regions as $region_name => $region_title) {

    if (isset($regions[$region_name])) {

      $region_content = '';
      $region = $regions[$region_name];

      // Loop through all fields after ordering on weight.
      asort($region);
      foreach ($region as $key => $weight) {
        $region_content .= isset($vars[$key .'_rendered']) ? $vars[$key .'_rendered'] : (isset($node->content[$key]['#value']) ? filter_xss($node->content[$key]['#value'], array('a', 'div', 'h1', 'h2', 'ul', 'li', 'table', 'tr', 'td', 'th')) : '');
        unset($node->content[$key]);
      }

      // Render region.
      if (!empty($region_content)) {
        if ($region_name == 'left' || $region_name == 'right') {
          $region_classes[] = $region_name;
        }
        $themed_regions[$region_name] = $region_content;
      }
      else {
        $render = _nd_default_value($nd_display_settings, $node->build_mode, 'regions', 'render', $region_name, FALSE);
        if ($render == TRUE) {
          $themed_regions[$region_name] = '&nbsp;';
          if ($region_name == 'left' || $region_name == 'right') {
            $region_classes[] = $region_name;
          }
        }
      }
    }
    else {
      $render = _nd_default_value($nd_display_settings, $node->build_mode, 'regions', 'render', $region_name, FALSE);
      if ($render == TRUE) {
        $themed_regions[$region_name] = '&nbsp;';
        if ($region_name == 'left' || $region_name == 'right') {
          $region_classes[] = $region_name;
        }
      }
    }
  }

  $content = '';
  foreach ($themed_regions as $region_name => $region_content) {
    $content .= '<div class="nd-region-'. $region_name .'">'. $region_content .'</div>';
  }

  // Add classes based on node regions.
  $nd_classes = 'buildmode-'. str_replace('_', '-', $node->build_mode);
  if (in_array('left', $region_classes) && in_array('right', $region_classes)) {
    $nd_classes .= ' two-sidebars';
  }
  elseif (in_array('left', $region_classes) && !in_array('right', $region_classes)) {
    $nd_classes .= ' one-sidebar sidebar-left';
  }
  elseif (!in_array('left', $region_classes) && in_array('right', $region_classes)) {
    $nd_classes .= ' one-sidebar sidebar-right';
  }
  else {
    $nd_classes .= ' no-sidebars';
  }

  $vars['nd_classes'] = $nd_classes;
  $vars['content'] = $content;
}

/**
 * Implementation of hook_menu().
 */
function nd_menu() {
  $items = array();

  $build_modes = nd_get_build_modes(NULL, TRUE);
  if (!empty($build_modes)) {
    //if (!defined('MAINTENANCE_MODE') && variable_get('content_schema_version', -1) >= 6007) {
      foreach (node_get_types() as $type) {
        $type_name = $type->type;
        $type_url_str = str_replace('_', '-', $type_name);

        $items['admin/content/node-type/'. $type_url_str .'/display'] = array(
          'title' => 'Display fields',
          'page callback' => 'drupal_get_form',
          'page arguments' => array('nd_display_overview_form', $type_name),
          'access arguments' => array('administer content types'),
          'file' => 'includes/nd.display.inc',
          'type' => MENU_LOCAL_TASK,
          'weight' => 2,
        );

        // Add build modes from nd.
        $weight = 20;
        foreach ($build_modes as $key => $value) {
          $items['admin/content/node-type/'. $type_url_str .'/display/'. $key] = array(
            'title' => $value['title'],
            'page callback' => 'drupal_get_form',
            'page arguments' => array('nd_display_overview_form', $type_name, "$key"),
            'access arguments' => array('administer content types'),
            'type' => $key == 'full' ? MENU_DEFAULT_LOCAL_TASK : MENU_LOCAL_TASK,
            'file' => 'includes/nd.display.inc',
            'weight' => (isset($value['weight']) && !empty($value['weight'])) ? $value['weight'] : $weight++,
          );
        }
      }
    //}

    // Settings page.
    $items['admin/content/types/nd'] = array(
      'title' => 'Node displays',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('nd_settings'),
      'access arguments' => array('administer content types'),
      'file' => 'includes/nd.admin.inc',
      'type' => MENU_LOCAL_TASK,
      'weight' => 12,
    );
    $items['admin/content/types/nd/settings'] = array(
      'title' => 'Settings',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => 0,
    );

    // Build modes.
    $items['admin/content/types/nd/buildmodes'] = array(
      'title' => 'Build modes',
      'page callback' => 'nd_build_modes',
      'access arguments' => array('administer content types'),
      'file' => 'includes/nd.buildmodes.inc',
      'type' => MENU_LOCAL_TASK,
      'weight' => 1,
    );

    // Fields.
    $items['admin/content/types/nd/fields'] = array(
      'title' => 'Fields',
      'page callback' => 'nd_fields',
      'access arguments' => array('administer content types'),
      'file' => 'includes/nd.fields.inc',
      'type' => MENU_LOCAL_TASK,
      'weight' => 2,
    );
  }

  return $items;
}

/**
 * Implementation of hook_theme().
 */
function nd_theme() {
  $path = drupal_get_path('module', 'nd');

  $theme_functions = array(
    // Display overview form.
    'nd_display_overview_form' => array(
      'template' => 'nd-display-overview-form',
      'file' => 'theme.inc',
      'path' => $path .'/theme',
      'arguments' => array('form' => NULL),
    ),
    // Build modes matrix
    'nd_buildmodes_matrix_form' => array(
      'arguments' => array('form' => NULL),
      'file' => 'nd.buildmodes.inc',
      'path' => $path .'/includes',
    ),
    // Formatter theme functions.
    'nd_bodyfield_teaser' => array(
      'file' => 'formatters.inc',
      'path' => $path .'/formatters',
      'arguments' => array('node' => NULL),
    ),
    'nd_bodyfield_full' => array(
      'file' => 'formatters.inc',
      'path' => $path .'/formatters',
      'arguments' => array('node' => NULL),
    ),
    'nd_title_link' => array(
      'file' => 'formatters.inc',
      'path' => $path .'/formatters',
      'arguments' => array('node' => NULL),
    ),
    'nd_title_h1_link' => array(
      'file' => 'formatters.inc',
      'path' => $path .'/formatters',
      'arguments' => array('node' => NULL),
    ),
    'nd_title_nolink' => array(
      'file' => 'formatters.inc',
      'path' => $path .'/formatters',
      'arguments' => array('node' => NULL),
    ),
    'nd_title_h2_nolink' => array(
      'file' => 'formatters.inc',
      'path' => $path .'/formatters',
      'arguments' => array('node' => NULL),
    ),
    'nd_title_block' => array(
      'file' => 'formatters.inc',
      'path' => $path .'/formatters',
      'arguments' => array('node' => NULL),
    ),
    'nd_title_notitle' => array(
      'file' => 'formatters.inc',
      'path' => $path .'/formatters',
      'arguments' => array('node' => NULL),
    ),
    'nd_author_link' => array(
      'file' => 'formatters.inc',
      'path' => $path .'/formatters',
      'arguments' => array('node' => NULL),
    ),
    'nd_author_nolink' => array(
      'file' => 'formatters.inc',
      'path' => $path .'/formatters',
      'arguments' => array('node' => NULL),
    ),
  );

  return $theme_functions;

}

/**
 * API function to get all fields.
 */
function nd_get_fields($node_type, $has_body, $build_mode) {
  static $static_fields = array();
  if (!isset($static_fields[$node_type][$build_mode])) {

    // Fields in code.
    $fields = module_invoke_all('nd_fields', $node_type, $has_body, $build_mode);

    // Fields via the UI.
    $db_fields = variable_get('nd_fields', array());
    if (!empty($db_fields)) {
      foreach ($db_fields as $key => $field) {
        $fields[$key] = array(
          'title' => check_plain($field['title']),
          'code' => $field['code'],
          'type' => $field['type'],
        );
        $exclude = (isset($field['exclude'][$node_type]) && $field['exclude'][$node_type] === $node_type) ? TRUE : FALSE;
        if ($exclude) {
          unset($fields[$key]);
        }
      }
    }

    // Give modules a change to alter fields.
    drupal_alter('nd_fields', $fields);

    $static_fields[$node_type][$build_mode] = $fields;
  }
  return $static_fields[$node_type][$build_mode];
}

/**
 * Implementation of hook_nd_fields().
 *
 * @param string $node_type The name of the node type.
 * @param boolean $has_body Whether the node type has a body field or not.
 * @param string $build_mode The node build mode.
 */
function nd_nd_fields($node_type, $has_body, $build_mode) {

  $fields = array(
    'title' => array(
      'title' => t('Title'),
      'formatters' => array(
        'nd_title_nolink' => t('H1 title'),
        'nd_title_h2_nolink' => t('H2 title'),
        'nd_title_h1_link' => t('H1 title, linked to node'),
        'nd_title_link' => t('H2 title, linked to node'),
        'nd_title_block' => t('H2 block title, linked to node'),
        'nd_title_notitle' => t('Paragraph (no title), linked to node'),
      ),
      'type' => ND_FIELD_THEME,
    ),
    'author' => array(
      'title' => t('Author'),
      'formatters' => array(
        'nd_author_nolink' => t('Author'),
        'nd_author_link' => t('Author linked to profile')
      ),
      'type' => ND_FIELD_THEME,
    ),
    'links' => array(
      'title' => t('Links'),
      'div' => 'field links',
      'type' => ND_FIELD_PREPROCESS,
    ),
    'read_more' => array(
      'title' => t('Read more'),
      'code' => '<?php echo l(t("Read more"), "node/$node->nid"); ?>',
      'type' => ND_FIELD_OVERRIDABLE,
    ),
  );

  // Check for body.
  if ($has_body == TRUE) {
    $fields['body'] = array(
      'title' => t('Core body'),
      'formatters' => array(
        'nd_bodyfield_teaser' => t('Teaser'),
        'nd_bodyfield_full' => t('Full')
      ),
      'type' => ND_FIELD_THEME,
    );
  }

  if (module_exists('taxonomy')) {
    $fields['terms'] = array(
      'title' => t('Taxonomy'),
      'div' => 'field terms',
      'type' => ND_FIELD_PREPROCESS,
    );
  }

  if (module_exists('upload') && $build_mode != 'teaser' && variable_get("upload_$node_type", 1)) {
    $fields['files'] = array(
      'title' => t('Core upload'),
      'type' => ND_FIELD_IGNORE,
    );
  }

  return $fields;
}

/**
 * Api function to return all build modes.
 *
 * @param string $selector Return one build mode.
 * @param boolean $reset Whether to reset the build modes.
 * @return array All or one build mode(s).
 */
function nd_get_build_modes($selector = NULL, $reset = FALSE) {

  $build_modes = variable_get('nd_all_build_modes', array());

  if (empty($build_modes) || $reset) {
    // Define array with build modes to exclude.
    $exclude_build_modes = array(
      NODE_BUILD_SEARCH_INDEX,
      NODE_BUILD_RSS,
      'token',
    );

    // Build modes defined in hooks.
    $weight = 10;
    $build_modes = array();
    foreach (module_implements('content_build_modes') as $module) {
      $module_data = array();
      $function = $module .'_content_build_modes';
      $temp_data = $function();

      // We need to do some fiddling existing build modes.
      foreach ($temp_data as $tab_key => $value) {
        foreach ($value['build modes'] as $key => $value2) {

          // Exclude some build modes.
          if (in_array($key, $exclude_build_modes)) {
            continue;
          }

          // Sane build modes.
          $module_data[$key] = array(
            'weight' => isset($value['weight']) ? $value['weight'] : $weight++,
            'title' => $value2['title'],
            'build modes' => array(
              $key => array(
                'title' => $value2['title'],
              ),
            ),
          );

          // Check on Full node for its weight.
          if ($value2['title'] == t('Full node')) {
            $module_data[$key]['weight'] = -1;
          }
        }
      }
      // Merge the result
      $build_modes += $module_data;
    }

    // Give modules a change to alter build_modes.
    drupal_alter('nd_buildmodes', $build_modes);

    // Let's order.
    asort($build_modes);

    // Cache all build modes.
    variable_set('nd_all_build_modes', $build_modes);
  }

  if ($selector != NULL) {
    return $build_modes[$selector]['build modes'];
  }
  else {
    return $build_modes;
  }
}

/**
 * Implementation of hook_content_build_modes().
 */
function nd_content_build_modes() {

  $build_modes = array(
    'full' => array(
      'title' => t('Full node'),
      'weight' => -1,
      'build modes' => array(
        'full' => array(
          'title' => t('Full node'),
          'views style' => TRUE,
        ),
      ),
    ),
    'teaser' => array(
      'title' => t('Teaser'),
      'weight' => 1,
      'build modes' => array(
        'teaser' => array(
          'title' => t('Teaser'),
          'views style' => TRUE,
        ),
      ),
    ),
    'nd_sticky' => array(
      'title' => t('Sticky'),
      'weight' => 2,
      'build modes' => array(
        'nd_sticky' => array(
          'title' => t('Sticky'),
          'views style' => TRUE,
        ),
      ),
    ),
  );

  // Custom build modes through the UI.
  $weight = 10;
  $db_build_modes = variable_get('nd_build_modes', array());
  if (!empty($db_build_modes)) {
    foreach ($db_build_modes as $key => $name) {
      $build_modes[$key] = array(
        'title' => check_plain($name),
        'weight' => $weight++,
        'build modes' => array(
          $key => array(
            'title' => check_plain($name),
            'views style' => TRUE,
          ),
        ),
      );
    }
  }

  return $build_modes;
}

/**
 * Return array of available regions.
 */
function nd_regions() {
  return array(
    'header' => t('Header'),
    'left' => t('Left'),
    'middle' => t('Middle'),
    'right' => t('Right'),
    'footer' => t('Footer'),
    'disabled' => t('Disabled')
  );
}

/**
 * Return array of available label options.
 */
function nd_label_options() {
  return array(
    'above' => t('Above'),
    'inline' => t('Inline'),
    'hidden' => t('<Hidden>'),
  );
}

/**
 * Render code.
 *
 * @param string $key The name of the key to create.
 * @param array $value The field array.
 * @param stdClass The node object.
 */
function _nd_render_code($key, $value, &$node) {
  if (isset($value['code'])) {
    $node_key = $key .'_rendered';
    $value = _nd_eval($value['code'], $node);
    if (module_exists('token')) {
      $value = token_replace($value, 'node', $node);
    }
    // FIXME why is this also finding cck fields god damnit ?
    //$label_format = variable_get('display_'. $key .'_'. $node->build_mode .'_labelformat_'. $node->type, 'hidden');
    //drupal_set_message("$field - $label_format");
    $node->{$node_key} = '<div class="field field-'. $key .'">'. $value .'</div>';
  }
}

/**
 * Wrapper function around PHP eval(). We don't use drupal_eval
 * because custom fields might need properties from the current
 * node object.
 *
 * @param string $code The code to evaluate from the custom field.
 * @param stdClass $node The current node object.
 * @return string $output The output from eval.
 */
function _nd_eval($code, $node) {
  global $theme_path, $theme_info, $conf;

  // Store current theme path.
  $old_theme_path = $theme_path;

  // Restore theme_path to the theme, as long as drupal_eval() executes,
  // so code evaluted will not see the caller module as the current theme.
  // If theme info is not initialized get the path from theme_default.
  if (!isset($theme_info)) {
    $theme_path = drupal_get_path('theme', $conf['theme_default']);
  }
  else {
    $theme_path = dirname($theme_info->filename);
  }

  ob_start();
  print eval('?>'. $code);
  $output = ob_get_contents();
  ob_end_clean();

  // Recover original theme path.
  $theme_path = $old_theme_path;

  return $output;
}

/**
 * Function to load a default value for a content type.
 *
 * @param array $settings The settings loaded for a content type.
 * @param string $build_mode The name of the build mode.
 * @param string $type The name of the type to search (ie fields, regions)
 * @param string $key The name of the key to search in $type.
 * @param string $search_key The name of the key to search in $key.
 * @param string $default The default value.
 */
function _nd_default_value($settings, $build_mode, $type, $key, $search_key, $default) {
  return (isset($settings[$build_mode][$type][$key][$search_key])) ? $settings[$build_mode][$type][$key][$search_key] : $default;
}

/**
 * Function to load default form properties for a field in a context
 */
function _nd_field_default_form_properties($build_mode = NULL) {

  $field = array(
    'label' => array(
      'format' => array(
        '#type' => 'select',
        '#options' => nd_label_options(),
        '#default_value' => ND_DEFAULT_LABEL_FORMAT,
      ),
    ),
    'format' => array(
      '#type' => 'select',
      '#options' => array(),
      '#default_value' => ND_DEFAULT_FORMAT,
    ),
    'region' => array(
      '#type' => 'select',
      '#options' => nd_regions(),
      '#default_value' => ND_DEFAULT_REGION,
      '#attributes' => array(
        'class' => 'field-region-select field-region-'. ND_DEFAULT_REGION,
      ),
    ),
  );

  // If build mode is not empty, this means we're looking at
  // a content type which has no cck fields defined. Add
  // nd_weight and build mode key to the default field array
  // so the draghandling & formatters start working.
  if (!empty($build_mode)) {
    $field['nd_weight'] = array(
      '#type' => 'weight',
      '#default_value' => -19,
      '#delta' => 20,
    );
    $field[$build_mode] = array(
      'label' => array(
        'format' => array(
          '#type' => 'select',
          '#options' => array(
            'above' => 'Above',
            'inline' => 'Inline',
            'hidden' => '<Hidden>',
          ),
          '#default_value' => 'hidden',
        ),
      ),
      'format' => array(
        '#type' => 'select',
        '#options' => array(),
        '#default_value' => 'default',
      ),
      'region' => array (
        '#type' => 'select',
        '#options' => nd_regions(),
        '#default_value' => '',
        '#attributes' => array(
          'class' => 'field-region-select field-region-disabled',
        ),
      ),
    );
  }

  return $field;
}
