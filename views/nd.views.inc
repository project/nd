<?php

/**
 * @file
 * Views file for nd.
 */

/**
 * Implementation of hook_views_plugins
 */
function nd_views_plugins() {
  $path = drupal_get_path('module', 'nd');
  return array(
    'module' => 'nd', // This just tells our themes are elsewhere.
    'row' => array(
      'nd' => array(
        'title' => t('Node displays mode'),
        'help' => t('Display the node with node view.'),
        'handler' => 'views_plugin_platform_nd_node_view',
        'theme' => 'platform_nd_row_node',
        'base' => array('node'), // only works with 'node' as base.
        'path' => $path .'/views',
        'theme path' => $path .'/theme',
        'theme file' => 'theme.inc',
        'uses options' => TRUE,
        'type' => 'normal',
        'help topic' => 'style-node',
      ),
    ),
  );
}
