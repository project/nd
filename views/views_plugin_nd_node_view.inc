<?php

/**
 * @file
 * Contains the nd view style plugin.
 */

/**
 * Plugin which performs a node_view on the resulting object based on our nd build modes.
 */
class views_plugin_nd_node_view extends views_plugin_row {
  function option_definition() {
    $options = parent::option_definition();

    $options['build_mode'] = array('default' => 'teaser');

    return $options;
  }

  function options_form(&$form, &$form_state) {

    $build_mode_options = array();
    $build_modes = nd_get_build_modes();
    foreach ($build_modes as $build_mode => $value) {
      $build_mode_options[$build_mode] = $value['title'];
    }

    $form['build_mode'] = array(
      '#type' => 'radios',
      '#title' => t('Select the build mode'),
      '#default_value' => $this->options['build_mode'],
      '#options' => $build_mode_options,
    );
  }
}
