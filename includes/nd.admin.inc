<?php

/**
 * @file
 * Administrative functions for node displays module.
 */

/**
 * Settings page.
 */
function nd_settings($form_state) {
  $form = array();

  $form['regions_css'] = array(
    '#type' => 'fieldset',
    '#title' => t('Regions css'),
  );
  $form['regions_css']['nd_regions_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Load region css.'),
    '#description' => t('Load the default region css that comes with this module. For inspiration, look into css/regions.css. Note, do not make any changes in that file as module upgrades will overwrite your changes.'),
    '#default_value' => variable_get('nd_regions_css', TRUE),
  );

  // One button to save them all.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
    '#weight' => 20
  );

  return $form;
}

/**
 * Settings submit.
 */
function nd_settings_submit($form, &$form_state) {
  variable_set('nd_regions_css', $form_state['values']['nd_regions_css']);
  variable_set('nd_overrule_search', $form_state['values']['nd_overrule_search']);
  drupal_set_message(t('The configuration options have been saved.'));
  menu_rebuild();
}
