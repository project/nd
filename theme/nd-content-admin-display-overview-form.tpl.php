<?php

if ($rows): ?>
  <table id="content-display-overview" class="sticky-enabled">
    <thead>
      <tr>
        <th><?php print t('Field'); ?></th>
        <th><?php print t('Label'); ?></th>
        <?php foreach ($contexts as $key => $value): ?>
          <th><?php print $value['title']; ?> format</th>
          <th><?php print t('Exclude'); ?></th>
        <?php endforeach; ?>
        <th><?php print t('Region'); ?></th>
        <th><?php print t('Weight'); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php
      $count = 0;
      foreach ($rows as $row): ?>
        <tr class="<?php print $count % 2 == 0 ? 'odd' : 'even'; ?> <?php print $row->class ?>">
          <td><?php print $row->indentation; ?><span class="<?php print $row->label_class; ?>"><?php print $row->human_name; ?></span></td>
          <?php foreach ($contexts as $context => $title): ?>
            <td><?php print $row->{$context}->label; ?></td>
            <td><?php print $row->{$context}->format; ?></td>
            <td><?php print $row->{$context}->exclude; ?></td>
          <?php endforeach; ?>
          <td><?php print $row->{$context}->region; ?></td>
          <td><?php print $row->{$contexts_order_key}; ?></td>
        </tr>
        <?php $count++;
      endforeach; ?>
    </tbody>
  </table>
  <?php print $submit; ?>
<?php endif; ?>