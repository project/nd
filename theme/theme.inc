<?php

/**
 * @file
 * Theming functions for nd.
 */

/**
 * Template helper for theme_views_view_row_node
 */
function template_preprocess_nd_row_node(&$vars) {
  $options = $vars['options'];
  $vars['node'] = ''; // make sure var is defined.
  $nid = $vars['row']->nid;
  if (!is_numeric($nid)) {
    return;
  }

  $node = node_load($nid);
  $node->build_mode = $options['build_mode'];
  $node->view = $vars['view'];

  if (empty($node)) {
    return;
  }

  $vars['node'] = node_view($node, FALSE, FALSE, FALSE);
}


/**
 * Theme preprocess function for content-admin-field-overview-form.tpl.php.
 */
function template_preprocess_nd_content_field_overview_form(&$vars) {
  $form = &$vars['form'];

  $order = _content_overview_order($form, $form['#field_rows'], $form['#group_rows']);
  $rows = array();

  // Identify the 'new item' keys in the form, they look like
  // _add_new_field, add_new_group.
  $keys = array_keys($form);
  $add_rows = array();
  foreach ($keys as $key) {
    if (substr($key, 0, 4) == '_add') {
      $add_rows[] = $key;
    }
  }
  while ($order) {
    $key = reset($order);
    $element = &$form[$key];

    // Only display the 'Add' separator if the 'add' rows are still
    // at the end of the table.
    if (!isset($added_separator)) {
      $remaining_rows = array_diff($order, $add_rows);
      if (empty($remaining_rows) && empty($element['#depth'])) {
        $row = new stdClass();
        $row->row_type = 'separator';
        $row->class = 'region';//'tabledrag-leaf region';
        $rows[] = $row;
        $added_separator = TRUE;
      }
    }

    $row = new stdClass();

    // Add target classes for the tabledrag behavior.
    $element['hidden_name']['#attributes']['class'] = 'field-name';
    // Add target classes for the update selects behavior.
    switch ($element['#row_type']) {
      case 'add_new_field':
        $element['type']['#attributes']['class'] = 'content-field-type-select';
        $element['widget_type']['#attributes']['class'] = 'content-widget-type-select';
        break;
      case 'add_existing_field':
        $element['field_name']['#attplatform_content_display_overview_formributes']['class'] = 'content-field-select';
        $element['widget_type']['#attributes']['class'] = 'content-widget-type-select';
        $element['label']['#attributes']['class'] = 'content-label-textfield';
        break;
    }
    foreach (element_children($element) as $child) {
      $row->{$child} = drupal_render($element[$child]);
    }
    $row->label_class = 'label-'. strtr($element['#row_type'], '_', '-');
    $row->row_type = $element['#row_type'];
    $row->indentation = theme('indentation', isset($element['#depth']) ? $element['#depth'] : 0);
    $row->class .= isset($element['#disabled_row']) ? ' menu-disabled' : '';
    $row->class .= isset($element['#add_new']) ? ' content-add-new' : '';
    $row->class .= isset($element['#leaf']) ? ' tabledrag-leaf' : '';
    $row->class .= isset($element['#root']) ? ' tabledrag-root' : '';

    $rows[] = $row;
    array_shift($order);
  }
  $vars['rows'] = $rows;
  $vars['submit'] = drupal_render($form);

  // Add settings for the update selects behavior.
  $js_fields = array();
  foreach (array_keys(content_existing_field_options($form['#type_name'])) as $field_name) {
    $field = content_fields($field_name);
    $js_fields[$field_name] = array('label' => $field['widget']['label'], 'type' => $field['type'], 'widget' => $field['widget']['type']);
  }
  drupal_add_js(array('contentWidgetTypes' => content_widget_type_options(), 'contentFields' => $js_fields), 'setting');
  drupal_add_js(drupal_get_path('module', 'content') .'/content.js');
}

/**
 * Theme preprocess function for nd-content-admin-display-overview-form.tpl.php.
 *
 * We are overriding the complete theming function to add
 * weights and regions which will be saved automatically in the display settings.
 */
function template_preprocess_nd_content_display_overview_form(&$vars) {
  $form = &$vars['form'];

  $contexts_selector = $form['#contexts'];
  $vars['basic'] = $contexts_selector == 'basic';
  $vars['contexts'] = nd_get_build_modes($contexts_selector);

  $order = _content_overview_order($form, $form['#fields'], $form['#groups']);
  if (empty($order)) {
    $vars['rows'] = array();
    $vars['submit'] = '';
    return;
  }

  $rows = array();
  foreach ($order as $key) {
    $element = &$form[$key];
    $row = new stdClass();

    $element[$form['contexts_order_key']['#value']]['#attributes']['class'] = 'field-'. $form['contexts_order_key']['#value'];

    foreach (element_children($element) as $child) {
      if (!array_key_exists('exclude', $element[$child])) {
        // Process weight.
        if ($child == $form['contexts_order_key']['#value']) {
          $element[$child] = process_weight($element[$child]);
        }
        $row->{$child} = drupal_render($element[$child]);
      }
      else {
        $row->{$child}->label = drupal_render($element[$child]['label']);
        $row->{$child}->format = drupal_render($element[$child]['format']);
        $row->{$child}->exclude = drupal_render($element[$child]['exclude']);
        $row->{$child}->region = drupal_render($element[$child]['region']);
      }

    }

    // Add draggable.
    $row->class = 'draggable';

    // Parent.
    $row->parent = 0;

    $row->label_class = in_array($key, $form['#groups']) ? 'label-group' : 'label-field';
    $row->indentation = theme('indentation', isset($element['#depth']) ? $element['#depth'] : 0);
    $rows[] = $row;
  }

  drupal_add_tabledrag('content-display-overview', 'order', 'sibling', 'field-'. $form['contexts_order_key']['#value']);

  $vars['contexts_order_key'] = $form['contexts_order_key']['#value'];
  $vars['rows'] = $rows;
  $vars['submit'] = drupal_render($form);
}
